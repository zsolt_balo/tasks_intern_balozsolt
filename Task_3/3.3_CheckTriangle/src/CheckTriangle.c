/*
 * CalculateDistance.c
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#include "CheckTriangle.h"

TriangleForm_Type CheckTriangle(TriangleDimensions_Type Dimensions)
{
   TriangleForm_Type result;

   // No parameter validity check. Negative side lengths do not make sense.
   if (((Dimensions.A + Dimensions.B) > Dimensions.C) &&
      ((Dimensions.A + Dimensions.C) > Dimensions.B) &&
      ((Dimensions.B + Dimensions.C) > Dimensions.A))
   {
      if ((Dimensions.A == Dimensions.B) &&
         (Dimensions.A == Dimensions.C) &&
         (Dimensions.B == Dimensions.C))
      {
         result = TRIANGLE_FORM_EQUILATERAL;
      }
      else if ((Dimensions.A == Dimensions.B) ||
         (Dimensions.A == Dimensions.C) ||
         (Dimensions.B == Dimensions.C))
      {
         if (((CALCULATE_POWER_OF_TWO(Dimensions.A) + CALCULATE_POWER_OF_TWO(Dimensions.B))
            == CALCULATE_POWER_OF_TWO(Dimensions.C))
            ||
            ((CALCULATE_POWER_OF_TWO(Dimensions.B) + CALCULATE_POWER_OF_TWO(Dimensions.C))
               == CALCULATE_POWER_OF_TWO(Dimensions.A))
            ||
            ((CALCULATE_POWER_OF_TWO(Dimensions.A) + CALCULATE_POWER_OF_TWO(Dimensions.C))
               == CALCULATE_POWER_OF_TWO(Dimensions.B)))
         {
            result = TRIANGLE_FORM_RIGHT_ISOSCELES;
         }
         else
         {
            result = TRIANGLE_FORM_ISOSCELES;
         }
      }
      else
      {
         if (((CALCULATE_POWER_OF_TWO(Dimensions.A) + CALCULATE_POWER_OF_TWO(Dimensions.B))
            == CALCULATE_POWER_OF_TWO(Dimensions.C))
            ||
            ((CALCULATE_POWER_OF_TWO(Dimensions.B) + CALCULATE_POWER_OF_TWO(Dimensions.C))
               == CALCULATE_POWER_OF_TWO(Dimensions.A))
            ||
            ((CALCULATE_POWER_OF_TWO(Dimensions.A) + CALCULATE_POWER_OF_TWO(Dimensions.C))
               == CALCULATE_POWER_OF_TWO(Dimensions.B)))
         {
            result = TRIANGLE_FORM_RIGHT_SCALENE;
         }
         else
         {
            result = TRIANGLE_FORM_SCALENE;
         }
      }
   }
   else
   {
      result = TRIANGLE_FORM_IMPOSSIBILE;
   }
   return result;
}
