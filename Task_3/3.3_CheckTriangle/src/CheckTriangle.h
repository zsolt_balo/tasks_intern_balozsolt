/*
 * CalculateDistance.h
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#ifndef CHECKTRIANGLE_H_
#define CHECKTRIANGLE_H_

#include "Compiler.h"
#include "Platform_Types.h"
#include "Std_Types.h"

// Shall not be visible from outside the module. The specifications did not ask for this macro, it is implementation
// specific so it should be moved to CheckTriangle.c
#define CALCULATE_POWER_OF_TWO(a)(a*a)

typedef enum
{
	TRIANGLE_FORM_IMPOSSIBILE,
	TRIANGLE_FORM_SCALENE,
	TRIANGLE_FORM_RIGHT_SCALENE,
	TRIANGLE_FORM_ISOSCELES,
	TRIANGLE_FORM_RIGHT_ISOSCELES,
	TRIANGLE_FORM_EQUILATERAL,
} TriangleForm_Type;

typedef struct
{
	sint8 A;
	sint8 B;
	sint8 C;
} TriangleDimensions_Type;

TriangleForm_Type CheckTriangle(TriangleDimensions_Type Dimensions);

#endif /* CHECKTRIANGLE_H_ */
