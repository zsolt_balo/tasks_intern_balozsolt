/*
 * Main.c
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#include <stdio.h>
#include "CheckTriangle.h"

/**
 * \brief      Prints the given enumeration numerical value as a string.
 * \param      t_TriangleForm: Enumeration numerical value which to be printed out as a string.
 * \return     -
 */
static void Main_v_PrintEnumString(TriangleForm_Type t_TriangleForm)
{
   switch (t_TriangleForm)
   {
      case TRIANGLE_FORM_IMPOSSIBILE:
      {
         printf("TRIANGLE_FORM_IMPOSSIBILE\n");
         break;
      }
      case TRIANGLE_FORM_SCALENE:
      {
         printf("TRIANGLE_FORM_SCALENE\n");
         break;
      }
      case TRIANGLE_FORM_RIGHT_SCALENE:
      {
         printf("TRIANGLE_FORM_RIGHT_SCALENE\n");
         break;
      }
      case TRIANGLE_FORM_ISOSCELES:
      {
         printf("TRIANGLE_FORM_ISOSCELES\n");
         break;
      }
      case TRIANGLE_FORM_RIGHT_ISOSCELES:
      {
         printf("TRIANGLE_FORM_RIGHT_ISOSCELES\n");
         break;
      }
      case TRIANGLE_FORM_EQUILATERAL:
      {
         printf("TRIANGLE_FORM_EQUILATERAL\n");
         break;
      }
      default:
         break;
   }
}

int main(int argc,char** argv)
{
   TriangleDimensions_Type t_Dimensions;

   t_Dimensions.A = 0;
   t_Dimensions.B = 0;
   t_Dimensions.C = 0;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 2;
   t_Dimensions.B = 2;
   t_Dimensions.C = -4;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 120;
   t_Dimensions.B = 60;
   t_Dimensions.C = 60;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 3;
   t_Dimensions.B = 4;
   t_Dimensions.C = 5;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 15;
   t_Dimensions.B = 20;
   t_Dimensions.C = 25;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 2;
   t_Dimensions.B = 2;
   t_Dimensions.C = 2;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 127;
   t_Dimensions.B = 127;
   t_Dimensions.C = 127;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 2;
   t_Dimensions.B = 2;
   t_Dimensions.C = 3;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 127;
   t_Dimensions.B = 127;
   t_Dimensions.C = 126;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 3;
   t_Dimensions.B = 4;
   t_Dimensions.C = 6;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   t_Dimensions.A = 120;
   t_Dimensions.B = 119;
   t_Dimensions.C = 118;
   Main_v_PrintEnumString(CheckTriangle(t_Dimensions));

   // Correct output is:
   //   TRIANGLE_FORM_IMPOSSIBILE
   //   TRIANGLE_FORM_IMPOSSIBILE
   //   TRIANGLE_FORM_IMPOSSIBILE
   //   TRIANGLE_FORM_RIGHT_SCALENE
   //   TRIANGLE_FORM_RIGHT_SCALENE
   //   TRIANGLE_FORM_EQUILATERAL
   //   TRIANGLE_FORM_EQUILATERAL
   //   TRIANGLE_FORM_ISOSCELES
   //   TRIANGLE_FORM_ISOSCELES
   //   TRIANGLE_FORM_SCALENE
   //   TRIANGLE_FORM_SCALENE
   // Correct implementation.
   return 0;
}
