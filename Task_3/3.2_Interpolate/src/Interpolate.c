/*
 * CalculateDistance.c
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#include "Interpolate.h"

sint8 Interpolate(sint8 x, Point_Type A, Point_Type B)
{
	sint8 result = 0x00;

	/*x nu poate fi din afara intervalului delimitat de puctele A si B*/
// Limitation not required. x can be outside the line that is defined by A and B.
	if(((x<A.x) && (x<B.x))||((x>A.x) && (x>B.x)))
	{
		return 0x80;
	}

	// Division by zero takes place. Overflow and underflow can take place.
   // Compiler helped you in this case (it performed the casts for you). Usually if you use operands of the same
   // type the compiler will assume that the result will be stored on the same data type.
   // This implementation does not work on every compiler out there, so you should add the proper casts for each
   // and every partial result.
	result = A.y + (((x-A.x)*(B.y-A.y))/(B.x-A.x));

	return result;

}
