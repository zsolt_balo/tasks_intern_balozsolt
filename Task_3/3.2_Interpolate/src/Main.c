/*
 * Main.c
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#include <stdio.h>
#include "Interpolate.h"

int main()
{
   Point_Type t_A, t_B;
   sint8 sc_X;

   t_A.x = 0;
   t_A.y = 0;
   t_B.x = 2;
   t_B.y = 2;
   sc_X = 20;
   printf("%d\n", Interpolate(sc_X, t_A, t_B));

   t_A.x = -128;
   t_A.y = -128;
   t_B.x = 127;
   t_B.y = 127;
   sc_X = 7;
   printf("%d\n", Interpolate(sc_X, t_A, t_B));

   t_A.x = -2;
   t_A.y = -2;
   t_B.x = 4;
   t_B.y = 9;
   sc_X = 2;
   printf("%d\n", Interpolate(sc_X, t_A, t_B));

   t_A.x = -3;
   t_A.y = 3;
   t_B.x = 3;
   t_B.y = -1;
   sc_X = 0;
   printf("%d\n", Interpolate(sc_X, t_A, t_B));

   t_A.x = -80;
   t_A.y = -20;
   t_B.x = 80;
   t_B.y = -20;
   sc_X = 0;
   printf("%d\n", Interpolate(sc_X, t_A, t_B));

   t_A.x = 127;
   t_A.y = 127;
   t_B.x = 0;
   t_B.y = -127;
   sc_X = -120;
   printf("%d\n", Interpolate(sc_X, t_A, t_B));

//   Crashes in this case.
//   t_A.x = 0;
//   t_A.y = -2;
//   t_B.x = 0;
//   t_B.y = 2;
//   sc_X = 0;
//   printf("%d\n", Interpolate(sc_X, t_A, t_B));

//   Correct output is:
//   20
//   7
//   5
//   1
//   -20
//   -111
//   -128

   return 0;
}
