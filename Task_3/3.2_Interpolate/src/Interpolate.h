/*
 * CalculateDistance.h
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#ifndef INTERPOLATE_H_
#define INTERPOLATE_H_

#include "Compiler.h"
#include "Platform_Types.h"
#include "Std_Types.h"

typedef struct
{
	sint8  x;
	sint8  y;
}Point_Type;

sint8 Interpolate(sint8 x, Point_Type A, Point_Type B);

#endif /* INTERPOLATE_H_ */
