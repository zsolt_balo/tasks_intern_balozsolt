/*
 * CalculateDistance.c
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#include "WriteComplexSum.h"

void WriteComplexSum(ComplexNumber_Type A, ComplexNumber_Type B)
{
	ComplexNumber_Type Result = {0x00,0x00,0x00};
	uint8 A_set_i = 0x00, B_set_i = 0x00;

	/*Check if i is needed in the final expression*/
// A faster way is A.iPow & 0x03U;
// A function would have made sense for this reduction algorithm (it is identical for both A and B).
	switch((A.iPow % 4))
	{
		case 0:
			A_set_i = 0x00;
			break;
		case 1:
			A_set_i = 0x01;
			break;
		case 2:
			A_set_i = 0x00;
			A.b = A.b*(-1);
			break;
		case 3:
			A_set_i = 0x01;
			A.b = A.b*(-1);
			break;
	}

	switch((B.iPow % 4))
	{
		case 0:
			B_set_i = 0x00;
			break;
		case 1:
			B_set_i = 0x01;
			break;
		case 2:
			B_set_i = 0x00;
			B.b = B.b*(-1);
			break;
		case 3:
			B_set_i = 0x01;
			B.b = B.b*(-1);
			break;
	}

	// Redundant checks. These reductions could have been performed in the previous switch conditions.
	// Overflow can take place.
	/*Sum calculation*/
	if((A_set_i == 1) && (B_set_i == 1))
	{
		Result.a = (A.a+B.a);
		Result.b = (A.b+B.b);
	}
	else if((A_set_i == 1) && (B_set_i == 0))
	{
		Result.a = (A.a+B.a+B.b);
		Result.b = A.b;
	}
	else if((A_set_i == 0) && (B_set_i == 1))
	{
		Result.a = (A.a+B.a+A.b);
		Result.b = B.b;
	}
	else
	{
		Result.a = (A.a+B.a)+(A.b+B.b);
		Result.b = 0;
	}

	/*Print mode selection*/
	if((Result.a != 0)&&(Result.b != 0))
	{
		if(Result.b>0)
		{
			if((A_set_i == 1)||(B_set_i == 1))
			{
				printf("%d+%di\n",Result.a,Result.b);
			}
			else
			{
				printf("%d+%d\n",Result.a,Result.b);
			}
		}
		else
		{
			if((A_set_i == 1)||(B_set_i == 1))
			{
				printf("%d%di\n",Result.a,Result.b);
			}
			else
			{
				printf("%d%d\n",Result.a,Result.b);
			}
		}
	}
	else if((Result.a == 0)&&(Result.b != 0))
	{
		if((A_set_i == 1)||(B_set_i == 1))
		{
			printf("%di\n",Result.b);
		}
		else
		{
			printf("%d\n",Result.b);
		}
	}
	else if((Result.a != 0)&&(Result.b == 0))
	{
		printf("%d\n",Result.a);
	}
	else
	{
		printf("0\n");
	}

}
