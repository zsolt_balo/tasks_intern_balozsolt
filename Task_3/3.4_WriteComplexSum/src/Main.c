/*
 * Main.c
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#include "WriteComplexSum.h"

int main()
{
   ComplexNumber_Type t_A, t_B;

   t_A.a = 0;
   t_A.b = 0;
   t_A.iPow = 3;
   t_B.a = 0;
   t_B.b = 0;
   t_B.iPow = 2;
   WriteComplexSum(t_A, t_B);

   t_A.a = 3;
   t_A.b = -3;
   t_A.iPow = 3;
   t_B.a = -9;
   t_B.b = 3;
   t_B.iPow = 3;
   WriteComplexSum(t_A, t_B);

   t_A.a = 80;
   t_A.b = -3;
   t_A.iPow = 1;
   t_B.a = -9;
   t_B.b = 3;
   t_B.iPow = 1;
   WriteComplexSum(t_A, t_B);

   t_A.a = 2;
   t_A.b = -40;
   t_A.iPow = 3;
   t_B.a = 0;
   t_B.b = -2;
   t_B.iPow = 8;
   WriteComplexSum(t_A, t_B);

   t_A.a = 2;
   t_A.b = 2;
   t_A.iPow = 3;
   t_B.a = 0;
   t_B.b = -2;
   t_B.iPow = 8;
   WriteComplexSum(t_A, t_B);

   t_A.a = 1;
   t_A.b = 3;
   t_A.iPow = 9;
   t_B.a = 2;
   t_B.b = -2;
   t_B.iPow = 1;
   WriteComplexSum(t_A, t_B);

   t_A.a = 1;
   t_A.b = 3;
   t_A.iPow = 7;
   t_B.a = 2;
   t_B.b = -2;
   t_B.iPow = 1;
   WriteComplexSum(t_A, t_B);

   t_A.a = -128;
   t_A.b = -128;
   t_A.iPow = 255;
   t_B.a = -128;
   t_B.b = -128;
   t_B.iPow = 255;
   WriteComplexSum(t_A, t_B);

   t_A.a = 127;
   t_A.b = 127;
   t_A.iPow = 0;
   t_B.a = 127;
   t_B.b = 127;
   t_B.iPow = 0;
   WriteComplexSum(t_A, t_B);

//   Correct output is:
//   0
//   -6
//   71
//   40i
//   -2i
//   3 + 1i
//   3 - 5i
//   -256 + 256i
//   508

   return 0;
}
