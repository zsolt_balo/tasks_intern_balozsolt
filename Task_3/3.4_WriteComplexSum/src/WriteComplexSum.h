/*
 * CalculateDistance.h
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */

#ifndef WRITECOMPLEXSUM_H_
#define WRITECOMPLEXSUM_H_

#include <stdio.h>
#include "Compiler.h"
#include "Platform_Types.h"
#include "Std_Types.h"

typedef struct
{
	sint8 a;
	sint8 b;
	uint8 iPow;
} ComplexNumber_Type;

void WriteComplexSum(ComplexNumber_Type A, ComplexNumber_Type B);

#endif /* WRITECOMPLEXSUM_H_ */
