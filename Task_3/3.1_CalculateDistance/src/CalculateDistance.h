/*
 * CalculateDistance.h
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */
// Source file template not used.
#ifndef CALCULATEDISTANCE_H_
#define CALCULATEDISTANCE_H_

#include "Compiler.h"
#include "Platform_Types.h"
#include "Std_Types.h"

// Implementation specific macro shall not be visible from outside the module. Move to CalculateDistance.c. Naming
// convention not respected.
#define CALCULATE_POWER_OF_TWO(a)(a*a)

// Missing comments. Use Auto-formatter.
typedef struct
{
	sint8  x;
	sint8  y;
}Point_Type;

// Compilation error. Be more careful next time. Review should not take place if the project is not buildable.
uint16 CalculateDistance(Point_Type A, Point_Type B);

#endif /* CALCULATEDISTANCE_H_ */
