/*
 * Main.c
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */
// Source file template not used.
#include <stdio.h>
#include "CalculateDistance.h"

// Missing comments.
int main()
{
   Point_Type t_A, t_B;

   t_A.x = 0;
   t_A.y = 0;
   t_B.x = 0;
   t_B.y = 0;
   printf("%d\n", CalculateDistance(t_A, t_B));

   t_A.x = 1;
   t_A.y = 1;
   t_B.x = 2;
   t_B.y = 2;
   printf("%d\n", CalculateDistance(t_A, t_B));

   t_A.x = -3;
   t_A.y = 0;
   t_B.x = 3;
   t_B.y = 0;
   printf("%d\n", CalculateDistance(t_A, t_B));

   t_A.x = -2;
   t_A.y = 2;
   t_B.x = -2;
   t_B.y = -5;
   printf("%d\n", CalculateDistance(t_A, t_B));

   t_A.x = -3;
   t_A.y = -6;
   t_B.x = 4;
   t_B.y = -3;
   printf("%d\n", CalculateDistance(t_A, t_B));

   t_A.x = 0;
   t_A.y = 127;
   t_B.x = 0;
   t_B.y = -128;
   printf("%d\n", CalculateDistance(t_A, t_B));

   t_A.x = -128;
   t_A.y = -128;
   t_B.x = 127;
   t_B.y = 127;
   printf("%d\n", CalculateDistance(t_A, t_B));

//   Correct output is:
//   0
//   1
//   6
//   7
//   8
//   255
//   361

   return 0;
}
