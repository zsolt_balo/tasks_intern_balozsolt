/*
 * CalculateDistance.c
 *
 *  Created on: Nov 23, 2018
 *      Author: zsolt.balo
 */
// Source file template not used.
#include "CalculateDistance.h"

// Missing comments
uint16 CalculateDistance(Point_Type A, Point_Type B)
{
   // Naming convention not respected. Use Auto-formatter.
	uint8 distance  = 0x00;
	uint8 base 	 	= 0x01;
	uint8 run 	 	= 0x01;
	uint8 aprox1 	= 0x00, aprox1_base = 0x00;
	uint8 aprox2 	= 0x00, aprox2_base = 0x00;
	uint16 result   	= 0x00;

	distance = CALCULATE_POWER_OF_TWO((A.x - B.x)) + CALCULATE_POWER_OF_TWO((A.y - B.y));

	// While (aprox2 < distance) would have been better. The if inside is not needed.
	while(run)
	{
// Overflow takes place.
		aprox1 = CALCULATE_POWER_OF_TWO(base);
//		aprox1_base and aprox2_base not needed. When exiting the while they are both set to base.
		aprox1_base = base;
		base++;
// Overflow takes place.
		aprox2 = CALCULATE_POWER_OF_TWO(base);
		aprox2_base = base;

		if(aprox2 >= distance)
		{
			run = 0x00;
		}
	}

	result = CALCULATE_POWER_OF_TWO((aprox2 - distance))>CALCULATE_POWER_OF_TWO((aprox1 - distance))? aprox1_base : aprox2_base;

	return result;
}
