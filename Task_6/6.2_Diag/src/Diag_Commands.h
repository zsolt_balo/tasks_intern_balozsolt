/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Diag_Commands.h
 *    \author     Zsolt Balo
 *    \brief      Declaration of the command prototypes
 *
 *    The user can configure in this file the number of commands and their prototypes
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef DIAG_COMMANDS_H_
#define DIAG_COMMANDS_H_

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  The number of commands defined by the user. */
#define DIAG_NR_OF_COMMANDS (7)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern boolean Diag_gb_Sum(uint8 * Payload, uint8 PayloadLength, uint8 * Response);
extern boolean Diag_gb_Multiplication(uint8 * Payload, uint8 PayloadLength, uint8 * Response);
extern boolean Diag_gb_Maximum(uint8 * Payload, uint8 PayloadLength, uint8 * Response);
extern boolean Diag_gb_Minimum(uint8 * Payload, uint8 PayloadLength, uint8 * Response);
extern boolean Diag_gb_Average(uint8 * Payload, uint8 PayloadLength, uint8 * Response);
extern boolean Diag_gb_Logical_OR(uint8 * Payload, uint8 PayloadLength, uint8 * Response);
extern boolean Diag_gb_Logical_AND(uint8 * Payload, uint8 PayloadLength, uint8 * Response);

/*-------------------------------------------------------------------------------------------------------------------*/

#endif /* DIAG_COMMANDS_H_ */
