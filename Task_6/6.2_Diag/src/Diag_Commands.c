/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Diag_Commands.c
 *    \author     Zsolt Balo
 *    \brief      Definition of the command prototypes
 *
 *    The file contains the implementation of the commands according to the requirements
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Diag.h"
#include "Diag_Commands.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
/**
 * \brief      Calculates the sum of the received payload bytes.
 * \param      Payload : The data array with the desired operation.
 * \param      PayloadLength : the number of operands used for the command.
 * \param      Response : The data array with the result of the check and the selected operation.
 * \return     If the length is less than 0xFFFF returns TRUE, otherwise FALSE
 */
boolean Diag_gb_Sum(uint8 * Payload, uint8 PayloadLength, uint8 * Response)
{
   uint8 uc_I = 0U;
   uint32 ul_Result = 0U;

   /*Realize the commands operation*/
   for (uc_I = 0U; uc_I < PayloadLength; uc_I++)
   {
      ul_Result += Payload[uc_I];
   }

   if (ul_Result < 0xFF)
   {
      Response[0] = 0x01;
      Response[1] = ul_Result;
   }
   else
   {
      Response[0] = 0x02;
      Response[1] = ((ul_Result & 0xFF00) >> 8);
      Response[2] = (ul_Result & 0xFF);
   }

   return TRUE;
}

/**
 * \brief      Calculates the product of the request payload bytes.
 * \param      Payload : The data array with the desired operation.
 * \param      PayloadLength : the number of operands used for the command.
 * \param      Response : The data array with the result of the check and the selected operation.
 * \return     If the length is less than 0xFFFF returns TRUE, otherwise FALSE
 */
boolean Diag_gb_Multiplication(uint8 * Payload, uint8 PayloadLength, uint8 * Response)
{
   uint8 uc_I = 0x00;
   boolean b_Result = FALSE;
   boolean b_OverflowDetection = FALSE;
   uint32 ul_Result2Send = 0x01, ul_Result2Send_new = 0x00, ul_Result2Send_old = 0x00;

   /*Realize the commands operation*/
   for (uc_I = 0x00; uc_I < PayloadLength; uc_I++)
   {
      ul_Result2Send_old = ul_Result2Send;
      ul_Result2Send *= Payload[uc_I];
      ul_Result2Send_new = ul_Result2Send;
      if ((ul_Result2Send_old > ul_Result2Send_new) && (Payload[uc_I] != 0x00))
      {
         b_OverflowDetection = TRUE;
      }
      else if (Payload[uc_I] == 0x00)
      {
         b_OverflowDetection = FALSE;
      }
      else
      {
         /*No Operation needed*/
      }
   }

   /*Create a response buffer according to the requirements*/
   if ((ul_Result2Send > 0xFFFF) || (b_OverflowDetection == TRUE))
   {
      b_Result = FALSE;

   }
   else if (ul_Result2Send < 0xFF)
   {

      b_Result = TRUE;
      Response[0] = 0x01;
      Response[1] = ul_Result2Send;

   }
   else
   {

      b_Result = TRUE;
      Response[0] = 0x02;
      Response[1] = ((ul_Result2Send & 0xFF00) >> 8);
      Response[2] = (ul_Result2Send & 0xFF);

   }

   return b_Result;
}

/**
 * \brief      Identifies the byte with the maximum value from the request payload bytes.
 * \param      Payload : The data array with the desired operation.
 * \param      PayloadLength : the number of operands used for the command.
 * \param      Response : The data array with the result of the check and the selected operation.
 * \return     If the length is less than 0xFFFF returns TRUE, otherwise FALSE
 */
boolean Diag_gb_Maximum(uint8 * Payload, uint8 PayloadLength, uint8 * Response)
{
   uint8 uc_I = 0x00;
   boolean b_Result = FALSE;
   uint32 ul_Result2Send = 0x00;

   for (uc_I = 0x00; uc_I < PayloadLength; uc_I++)
   {
      if (Payload[uc_I] > ul_Result2Send)
      {
         ul_Result2Send = Payload[uc_I];
      }
   }

   if (ul_Result2Send > 0xFF)
   {
      b_Result = FALSE;

   }
   else
   {
      b_Result = TRUE;
      Response[0] = 0x01;
      Response[1] = ul_Result2Send;
   }

   return b_Result;
}

/**
 * \brief      Identifies the byte with the minimum value from the request payload bytes.
 * \param      Payload : The data array with the desired operation.
 * \param      PayloadLength : the number of operands used for the command.
 * \param      Response : The data array with the result of the check and the selected operation.
 * \return     If the length is less than 0xFFFF returns TRUE, otherwise FALSE
 */
boolean Diag_gb_Minimum(uint8 * Payload, uint8 PayloadLength, uint8 * Response)
{
   uint8 uc_I = 0x00;
   boolean b_Result = FALSE;
   uint32 ul_Result2Send = 0xFF;

   for (uc_I = 0x00; uc_I < PayloadLength; uc_I++)
   {
      if (Payload[uc_I] < ul_Result2Send)
      {
         ul_Result2Send = Payload[uc_I];
      }
   }

   if (ul_Result2Send > 0xFF)
   {
      b_Result = FALSE;

   }
   else
   {
      b_Result = TRUE;
      Response[0] = 0x01;
      Response[1] = ul_Result2Send;
   }

   return b_Result;
}

/**
 * \brief      Calculates the average value of the request payload bytes.
 * \param      Payload : The data array with the desired operation.
 * \param      PayloadLength : the number of operands used for the command.
 * \param      Response : The data array with the result of the check and the selected operation.
 * \return     If the length is less than 0xFFFF returns TRUE, otherwise FALSE
 */
boolean Diag_gb_Average(uint8 * Payload, uint8 PayloadLength, uint8 * Response)
{

   uint8 uc_I = 0x00;
   boolean b_Result = FALSE;
   uint32 ul_Result2Send = 0x00;

   for (uc_I = 0x00; uc_I < PayloadLength; uc_I++)
   {
      ul_Result2Send += Payload[uc_I];
   }
   ul_Result2Send = ul_Result2Send / PayloadLength;

   if (ul_Result2Send > 0xFF)
   {
      b_Result = FALSE;

   }
   else
   {
      b_Result = TRUE;
      Response[0] = 0x01;
      Response[1] = ul_Result2Send;
   }

   return b_Result;
}

/**
 * \brief      Calculates the logical OR between the request payload bytes.
 * \param      Payload : The data array with the desired operation.
 * \param      PayloadLength : the number of operands used for the command.
 * \param      Response : The data array with the result of the check and the selected operation.
 * \return     If the length is less than 0xFFFF returns TRUE, otherwise FALSE
 */
boolean Diag_gb_Logical_OR(uint8 * Payload, uint8 PayloadLength, uint8 * Response)
{

   uint8 uc_I = 0x00;
   boolean b_Result = FALSE;
   uint32 ul_Result2Send = 0x00;

   for (uc_I = 0x00; uc_I < PayloadLength; uc_I++)
   {
      ul_Result2Send |= Payload[uc_I];
   }

   if (ul_Result2Send > 0xFF)
   {
      b_Result = FALSE;

   }
   else
   {
      b_Result = TRUE;
      Response[0] = 0x01;
      Response[1] = ul_Result2Send;
   }

   return b_Result;
}

/**
 * \brief      Calculates the logical AND between the request payload bytes.
 * \param      Payload : The data array with the desired operation.
 * \param      PayloadLength : the number of operands used for the command.
 * \param      Response : The data array with the result of the check and the selected operation.
 * \return     If the length is less than 0xFFFF returns TRUE, otherwise FALSE
 */
boolean Diag_gb_Logical_AND(uint8 * Payload, uint8 PayloadLength, uint8 * Response)
{

   uint8 uc_I = 0x00;
   boolean b_Result = FALSE;
   uint32 ul_Result2Send = 0xFF;

   for (uc_I = 0x00; uc_I < PayloadLength; uc_I++)
   {
      ul_Result2Send &= Payload[uc_I];
   }

   if (ul_Result2Send > 0xFF)
   {
      b_Result = FALSE;

   }
   else
   {
      b_Result = TRUE;
      Response[0] = 0x01;
      Response[1] = ul_Result2Send;
   }

   return b_Result;
}
