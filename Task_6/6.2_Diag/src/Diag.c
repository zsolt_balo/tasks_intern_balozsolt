/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Diag.c
 *    \author     Zsolt Balo
 *    \brief      The definition of the diagnostic handler
 *
 *    The main file of the module, which contains the handling function of the diag module.
 *    Its role is to processing of request diagnostic frames,by decoding and executing predefined
 *    commands based on the contents of a given buffer,and providing a response frame by writing
 *    the processing result in another given buffer.
 */

/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Diag.h"
#include "Diag_Table.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Diagnostic handling function.
 * \param      RequestFrame : The data array with the desired operation.
 * \param      ResponseFrame : The data array with the result of the check and the selected operation.
 * \return     -
 */
void Diag_Handle(uint8 * RequestFrame, uint8 * ResponseFrame)
{
   uint8 uc_I = 0x00, uc_ValidIdCheck = 0x00;
   boolean uc_CommandResult = FALSE;

   /*Check if, the RequestFrame ID and Length has a valid value*/

   if ((RequestFrame[1] < 7) && (RequestFrame[1] > 0))
   {
      for (uc_I = 0x00; uc_I < DIAG_NR_OF_COMMANDS; uc_I++)
      {
         if (RequestFrame[0] == Diag_gkat_Decoding_Table[uc_I].CommandID)
         {
            /*Set the valid id check flag, if the command was found in the list*/
            uc_ValidIdCheck = 0x01;

            uc_CommandResult = Diag_gkat_Decoding_Table[uc_I].Command((RequestFrame + 2),
               RequestFrame[1], (ResponseFrame + 1));
            if (uc_CommandResult == TRUE)
            {
               ResponseFrame[0] = DIAG_POSITIVE;

            }
            else
            {
               ResponseFrame[0] = DIAG_NEGATIVE_COMMAND_ERROR;
            }
            uc_I = DIAG_NR_OF_COMMANDS;
         }
         else
         {
            /*No operation*/
         }
      }
      if (uc_ValidIdCheck == 0x00)
      {
         ResponseFrame[0] = DIAG_NEGATIVE_COMMAND_NOT_FOUND;
      }
      else
      {
      }
   }
   else
   {
      ResponseFrame[0] = DIAG_NEGATIVE_INVALID_REQUEST;
   }
}
