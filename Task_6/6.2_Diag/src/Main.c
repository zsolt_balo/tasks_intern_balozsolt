/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Main.c
 *    \author     Zsolt Balo
 *    \brief      Main function declaration for testing the module
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include "Diag.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the request frame buffer. */
uint8 Main_auc_RequestFrame[8U] = { 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA };

/** \brief  Defines the response frame buffer. */
uint8 Main_auc_ResponseFrame[4U] = { 0xAA, 0xAA, 0xAA, 0xAA };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Implements the printing routine used for all test cases.
 * \param      -
 * \return     -
 */
void Main_v_PrintTestCase(void)
{
   uint8 uc_I;
   printf("Request:\t");
   for (uc_I = 0U; uc_I < 8U; uc_I++)
   {
      printf("0x%02X\t", Main_auc_RequestFrame[uc_I]);
   }
   printf("\nResponse:\t");
   for (uc_I = 0U; uc_I < 4U; uc_I++)
   {
      printf("0x%02X\t", Main_auc_ResponseFrame[uc_I]);
   }
   printf("\n\n");
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Implements the C main function - the first executed function in the program.
 * \param      -
 * \return     Returns 0 on program exit.
 */
int main()
{
   /* Sum test cases. */
   Main_auc_RequestFrame[0U] = 0x00U;
   Main_auc_RequestFrame[1U] = 0x00U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x00U;
   Main_auc_RequestFrame[1U] = 0x01U;
   Main_auc_RequestFrame[2U] = 0xABU;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x00U;
   Main_auc_RequestFrame[1U] = 0x03U;
   Main_auc_RequestFrame[2U] = 0xABU;
   Main_auc_RequestFrame[3U] = 0xABU;
   Main_auc_RequestFrame[4U] = 0xABU;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x00U;
   Main_auc_RequestFrame[1U] = 0x06U;
   Main_auc_RequestFrame[2U] = 0xFFU;
   Main_auc_RequestFrame[3U] = 0xFFU;
   Main_auc_RequestFrame[4U] = 0xFFU;
   Main_auc_RequestFrame[5U] = 0xFFU;
   Main_auc_RequestFrame[6U] = 0xFFU;
   Main_auc_RequestFrame[7U] = 0xFFU;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   /* Maximum test cases. */
   Main_auc_RequestFrame[0U] = 0x13U;
   Main_auc_RequestFrame[1U] = 0x06U;
   Main_auc_RequestFrame[2U] = 0xFAU;
   Main_auc_RequestFrame[3U] = 0xF0U;
   Main_auc_RequestFrame[4U] = 0x12U;
   Main_auc_RequestFrame[5U] = 0x32U;
   Main_auc_RequestFrame[6U] = 0x01U;
   Main_auc_RequestFrame[7U] = 0x09U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   /* Minimum test cases. */
   Main_auc_RequestFrame[0U] = 0x40U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   /* Average test cases. */
   Main_auc_RequestFrame[0U] = 0x55U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   /* Logical OR test cases. */
   Main_auc_RequestFrame[0U] = 0xA2U;
   Main_auc_RequestFrame[1U] = 0x06U;
   Main_auc_RequestFrame[2U] = 0x0BU;
   Main_auc_RequestFrame[3U] = 0x07U;
   Main_auc_RequestFrame[4U] = 0x03U;
   Main_auc_RequestFrame[5U] = 0x03U;
   Main_auc_RequestFrame[6U] = 0x87U;
   Main_auc_RequestFrame[7U] = 0x0AU;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   /* Logical AND test cases. */
   Main_auc_RequestFrame[0U] = 0xF2U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   /* Invalid command ID test cases/ */
   Main_auc_RequestFrame[0U] = 0xF7U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x04U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   /* Multiplication test cases. */
   Main_auc_RequestFrame[0U] = 0x12U;
   Main_auc_RequestFrame[1U] = 0x07U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x12U;
   Main_auc_RequestFrame[1U] = 0x01U;
   Main_auc_RequestFrame[2U] = 0x01U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x12U;
   Main_auc_RequestFrame[1U] = 0x02U;
   Main_auc_RequestFrame[2U] = 0xABU;
   Main_auc_RequestFrame[3U] = 0xABU;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x12U;
   Main_auc_RequestFrame[1U] = 0x06U;
   Main_auc_RequestFrame[2U] = 0x40U;
   Main_auc_RequestFrame[3U] = 0x40U;
   Main_auc_RequestFrame[4U] = 0x40U;
   Main_auc_RequestFrame[5U] = 0x40U;
   Main_auc_RequestFrame[6U] = 0x40U;
   Main_auc_RequestFrame[7U] = 0x04U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x12U;
   Main_auc_RequestFrame[1U] = 0x06U;
   Main_auc_RequestFrame[2U] = 0xFFU;
   Main_auc_RequestFrame[3U] = 0xFFU;
   Main_auc_RequestFrame[4U] = 0xFFU;
   Main_auc_RequestFrame[5U] = 0xFFU;
   Main_auc_RequestFrame[6U] = 0xFFU;
   Main_auc_RequestFrame[7U] = 0xFFU;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   Main_auc_RequestFrame[0U] = 0x12U;
   Main_auc_RequestFrame[1U] = 0x06U;
   Main_auc_RequestFrame[2U] = 0xFFU;
   Main_auc_RequestFrame[3U] = 0xFFU;
   Main_auc_RequestFrame[4U] = 0xFFU;
   Main_auc_RequestFrame[5U] = 0xFFU;
   Main_auc_RequestFrame[6U] = 0xFFU;
   Main_auc_RequestFrame[7U] = 0x00U;
   Diag_Handle(Main_auc_RequestFrame, Main_auc_ResponseFrame);
   Main_v_PrintTestCase();

   /*
    Correct output is:
    Request:   0x00  0x00  0xAA  0xAA  0xAA  0xAA  0xAA  0xAA
    Response:   0xFF  0xAA  0xAA  0xAA

    Request: 0x00  0x01  0xAB  0xAA  0xAA  0xAA  0xAA  0xAA
    Response:   0x00  0x01  0xAB  0xAA

    Request: 0x00  0x03  0xAB  0xAB  0xAB  0xAA  0xAA  0xAA
    Response:   0x00  0x02  0x02  0x01

    Request: 0x00  0x06  0xFF  0xFF  0xFF  0xFF  0xFF  0xFF
    Response:   0x00  0x02  0x05  0xFA

    Request: 0x13  0x06  0xFA  0xF0  0x12  0x32  0x01  0x09
    Response:   0x00  0x01  0xFA  0xFA

    Request: 0x40  0x06  0xFA  0xF0  0x12  0x32  0x01  0x09
    Response:   0x00  0x01  0x01  0xFA

    Request: 0x55  0x06  0xFA  0xF0  0x12  0x32  0x01  0x09
    Response:   0x00  0x01  0x5E  0xFA

    Request: 0xA2  0x06  0x0B  0x07  0x03  0x03  0x87  0x0A
    Response:   0x00  0x01  0x8F  0xFA

    Request: 0xF2  0x06  0x0B  0x07  0x03  0x03  0x87  0x0A
    Response:   0x00  0x01  0x02  0xFA

    Request: 0xF7  0x06  0x0B  0x07  0x03  0x03  0x87  0x0A
    Response:   0xFE  0x01  0x02  0xFA

    Request: 0x04  0x06  0x0B  0x07  0x03  0x03  0x87  0x0A
    Response:   0xFE  0x01  0x02  0xFA

    Request: 0x12  0x07  0x0B  0x07  0x03  0x03  0x87  0x0A
    Response:   0xFF  0x01  0x02  0xFA

    Request: 0x12  0x01  0x01  0x07  0x03  0x03  0x87  0x0A
    Response:   0x00  0x01  0x01  0xFA

    Request: 0x12  0x02  0xAB  0xAB  0x03  0x03  0x87  0x0A
    Response:   0x00  0x02  0x72  0x39

    Request: 0x12  0x06  0x40  0x40  0x40  0x40  0x40  0x04
    Response:   0xFD  0x02  0x72  0x39

    Request: 0x12  0x06  0xFF  0xFF  0xFF  0xFF  0xFF  0xFF
    Response:   0xFD  0x02  0x72  0x39

    Request: 0x12  0x06  0xFF  0xFF  0xFF  0xFF  0xFF  0x00
    Response:   0x00  0x01  0x00  0x39

    * */
   return 0;
}

