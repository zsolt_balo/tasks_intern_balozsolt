/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Main.c
 *    \author     Zsolt Balo
 *    \brief      Main function declaration for testing the module
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "stdio.h"
#include "Pattern.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (0U == PATTERN_TEST_CASE_SELECTOR)

/** \brief  Defines the search structure for test case 0. */
static Pattern_Type Main_t_SearchStructure = { 0xAA, 0x03, 0xAA, 0x03, 0xAA, 0xAA };

#elif (1U == PATTERN_TEST_CASE_SELECTOR)

/** \brief  Defines the search structure for test case 1. */
static Pattern_Type Main_t_SearchStructure =
{
   {  0x22, 0x22, 0x22, 0x07,},
   0x22072207,
   0xAA,
};

#elif (2U == PATTERN_TEST_CASE_SELECTOR)

/** \brief  Defines the search structure for test case 2. */
static Pattern_Type Main_t_SearchStructure =
{
   {
      {  0x03, 0x03, 0x03},
      {  0xAA, 0xAA, 0xAA},
      {  0x07, 0xAA, 0xAA},
   }
};

#endif

/** \brief  Defines the positions result buffer. */
static uint8 Main_auc_PatternPositions[PATTERN_MAXIMUM_NR_OF_POSITIONS];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static void Main_v_Print(uint8 uc_Pattern, uint8 uc_ReturnLength);

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Implements the printing routine used for all tests.
 * \param      uc_Pattern: The pattern used in the test.
 * \param      uc_ReturnLength: The length that was returned by the pattern search interface.
 * \return     -
 */
static void Main_v_Print(uint8 uc_Pattern, uint8 uc_ReturnLength)
{
   uint8 uc_I;
   printf("Pattern:  0x%02X\n", uc_Pattern);
   printf("Return:   %d\n", uc_ReturnLength);

   for (uc_I = 0U; uc_I < PATTERN_MAXIMUM_NR_OF_POSITIONS; uc_I++)
   {
      printf("%4d", Main_auc_PatternPositions[uc_I]);
   }
   printf("\n\n");
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Implements the C main function - the first executed function in the program.
 * \param      -
 * \return     Returns 0 on program exit.
 */
int main()
{
   printf("Structure length: %d\n\n", sizeof(Main_t_SearchStructure));
   Main_v_Print(0x00, Pattern_Find(Main_t_SearchStructure, 0x00, Main_auc_PatternPositions));
   Main_v_Print(0x03, Pattern_Find(Main_t_SearchStructure, 0x03, Main_auc_PatternPositions));
   Main_v_Print(0x07, Pattern_Find(Main_t_SearchStructure, 0x07, Main_auc_PatternPositions));
   Main_v_Print(0x22, Pattern_Find(Main_t_SearchStructure, 0x22, Main_auc_PatternPositions));
   Main_v_Print(0xAA, Pattern_Find(Main_t_SearchStructure, 0xAA, Main_auc_PatternPositions));

   return 0;
}

