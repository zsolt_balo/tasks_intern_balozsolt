/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pattern.c
 *    \author     Zsolt Balo
 *    \brief      Pattern searcher module interface implementation
 *
 *    The module looks up a given binary pattern from the provided array, counts each occurrence and saves the index of
 *    the found pattern in the result buffer.If the number of results is greater then the
 *    PATTERN_MAXIMUM_NR_OF_POSITIONS then PATTERN_ERROR_CODE is returned.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Pattern.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Pattern search function.
 * \param      Structure : The data structure used for the search of the pattern.
 * \param      Pattern : The byte pattern which need to be found.
 * \param      PosAddr : The array which stores the occurrence's position.
 * \return     the number of occurrences of the pattern
 */
uint8 Pattern_Find(Pattern_Type Structure, uint8 Pattern, uint8 * PosAddr)
{
   uint8 uc_Result = 0x00, uc_I = 0x00, uc_ResultBufferPosition = 0x00;

   /*Search for the desired pattern, and if found store its position.
    *If there are less values, then the buffer size, the remaining values
    *will have the don't care value.If there are more,
    *the PATTERN_ERROR_CODE will be returned*/

   for (uc_I = 0x00; uc_I < sizeof(Structure); uc_I++)
   {
      if ((*((uint8*) (&Structure) + uc_I)) == Pattern)
      {
         if (uc_ResultBufferPosition < PATTERN_MAXIMUM_NR_OF_POSITIONS)
         {
            (*(PosAddr + uc_ResultBufferPosition)) = uc_I;
            uc_Result = (uc_ResultBufferPosition + 1);
         }
         else
         {
            uc_Result = PATTERN_ERROR_CODE;
            uc_I = sizeof(Structure);
         }
         uc_ResultBufferPosition++;
      }
   }

   return uc_Result;
}
