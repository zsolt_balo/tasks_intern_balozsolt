/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pattern_Cfg.h
 *    \author     Zsolt Balo
 *    \brief      The pattern type and the test case is defined here
 *
 *    The user can configure the pattern used in the example, also the test case which will
 *    be used in the main function.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef PATTERN_CFG_H
#define PATTERN_CFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the test case to be executed. */
#define PATTERN_TEST_CASE_SELECTOR        (0U)

#if (0U == PATTERN_TEST_CASE_SELECTOR)
/** \brief  Defines the maximum number of byte positions to be used in test case 0. */
#define PATTERN_MAXIMUM_NR_OF_POSITIONS   (3U)

#elif (1U == PATTERN_TEST_CASE_SELECTOR)
/** \brief  Defines the maximum number of byte positions to be used in test case 1. */
#define PATTERN_MAXIMUM_NR_OF_POSITIONS   (4U)

#elif (2U == PATTERN_TEST_CASE_SELECTOR)
/** \brief  Defines the maximum number of byte positions to be used in test case 2. */
#define PATTERN_MAXIMUM_NR_OF_POSITIONS   (5U)

#endif

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (0U == PATTERN_TEST_CASE_SELECTOR)
/** \brief  Defines the search structure type for test case 0. */
typedef struct {
   /** \brief  Defines the first byte from the structure. */
   uint8 uc_Byte0;
   /** \brief  Defines the second byte from the structure. */
   uint8 uc_Byte1;
   /** \brief  Defines the third byte from the structure. */
   uint8 uc_Byte2;
   /** \brief  Defines the fourth byte from the structure. */
   uint8 uc_Byte3;
   /** \brief  Defines the fifth byte from the structure. */
   uint8 uc_Byte4;
   /** \brief  Defines the sixth byte from the structure. */
   uint8 uc_Byte5;
} Pattern_Type;

#elif (1U == PATTERN_TEST_CASE_SELECTOR)
/** \brief  Defines the search structure type for test case 1. */
typedef struct
{
   /** \brief  Defines the first member as a byte array with 4 elements. */
   uint8 auc_Array[4U];
   /** \brief  Defines the second member as a 4 bytes value. */
   uint32 ul_Long;
   /** \brief  Defines the third member as a byte. */
   uint8 uc_Byte;
}Pattern_Type;

#elif (2U == PATTERN_TEST_CASE_SELECTOR)
/** \brief  Defines an additional structure type for test case 2. */
typedef struct
{
   /** \brief  Defines the first member as a byte. */
   uint8 uc_Byte;
   /** \brief  Defines the second member as a 2 bytes value. */
   uint16 us_Short;
   /** \brief  Defines the third member as a 4 bytes value. */
   uint32 ul_Long;
}Pattern_AnotherType;

/** \brief  Defines the search structure type for test case 2. */
typedef struct
{
   /** \brief  Defines the structure member as a structure array with 3 elements. */
   Pattern_AnotherType at_StructArray[3U];
}Pattern_Type;
#endif

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/

#endif /* PATTERN_CFG_H_ */
