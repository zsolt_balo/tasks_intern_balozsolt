// Invalid file template
/*
 * ToDo.c
 *
 *  Created on: Nov 12, 2018
 *      Author: zsolt.balo
 */

// ToDo.c is not quite a good name.  Main.c sounds better.

#include "Task_2.h"
#include "stdio.h"

// Missing brief, param and return tags.
int main()
{
   printf("0xAA: %d\n", Task2_guc_VerifyPalindrome(0xAAU));
   printf("0xA5: %d\n", Task2_guc_VerifyPalindrome(0xA5U));
   printf("0x7E: %d\n", Task2_guc_VerifyPalindrome(0x7EU));
   printf("0x00: %d\n", Task2_guc_VerifyPalindrome(0x00U));
   printf("0x18: %d\n", Task2_guc_VerifyPalindrome(0x18U));
   printf("0xF4: %d\n", Task2_guc_VerifyPalindrome(0xF4U));
   printf("0xFF: %d\n", Task2_guc_VerifyPalindrome(0xFFU));
// Incorrect output. Correct output is:
//   0xAA: 0
//   0xA5: 1
//   0x7E: 1
//   0x00: 1
//   0x18: 1
//   0xF4: 0
//   0xFF: 1

   printf("\n\n");

   printf("0xAA: %d\n", Task2_guc_CountBitsOf1(0xAAU));
   printf("0xA5: %d\n", Task2_guc_CountBitsOf1(0xA5U));
   printf("0x7E: %d\n", Task2_guc_CountBitsOf1(0x7EU));
   printf("0x00: %d\n", Task2_guc_CountBitsOf1(0x00U));
   printf("0x18: %d\n", Task2_guc_CountBitsOf1(0x18U));
   printf("0xF4: %d\n", Task2_guc_CountBitsOf1(0xF4U));
   printf("0x00: %d\n", Task2_guc_CountBitsOf1(0x00U));
   printf("0xFF: %d\n", Task2_guc_CountBitsOf1(0xFFU));
// Output is correct.

   printf("\n\n");

   printf("0:\t%d\n", Task2_guc_VerifyPowerOf2(0U));
   printf("1:\t%d\n", Task2_guc_VerifyPowerOf2(1U));
   printf("3:\t%d\n", Task2_guc_VerifyPowerOf2(3U));
   printf("4:\t%d\n", Task2_guc_VerifyPowerOf2(4U));
   printf("7:\t%d\n", Task2_guc_VerifyPowerOf2(7U));
   printf("8:\t%d\n", Task2_guc_VerifyPowerOf2(8U));
   printf("12:\t%d\n", Task2_guc_VerifyPowerOf2(12U));
   printf("16:\t%d\n", Task2_guc_VerifyPowerOf2(16U));
   printf("32:\t%d\n", Task2_guc_VerifyPowerOf2(32U));
   printf("64:\t%d\n", Task2_guc_VerifyPowerOf2(64U));
   printf("73:\t%d\n", Task2_guc_VerifyPowerOf2(73U));
   printf("128:\t%d\n", Task2_guc_VerifyPowerOf2(128U));
   printf("160:\t%d\n", Task2_guc_VerifyPowerOf2(160U));
   printf("255:\t%d\n", Task2_guc_VerifyPowerOf2(255U));
// Incorrect output. Correct output is:
//   0: 0
//   1: 1
//   3: 0
//   4: 1
//   7: 0
//   8: 1
//   12:   0
//   16:   1
//   32:   1
//   64:   1
//   73:   0
//   128:  1
//   160:  0
//   255:  0
   printf("\n\n");

   printf("0x5C: %d\n", Task2_gsc_CalculateSumOfNibbles(0x5CU));
   printf("0x85: %d\n", Task2_gsc_CalculateSumOfNibbles(0x85U));
   printf("0xF2: %d\n", Task2_gsc_CalculateSumOfNibbles(0xF2U));
   printf("0x00: %d\n", Task2_gsc_CalculateSumOfNibbles(0x00U));
   printf("0xFF: %d\n", Task2_gsc_CalculateSumOfNibbles(0xFFU));
   printf("0x70: %d\n", Task2_gsc_CalculateSumOfNibbles(0x70U));
   printf("0x80: %d\n", Task2_gsc_CalculateSumOfNibbles(0x80U));
   printf("0x7F: %d\n", Task2_gsc_CalculateSumOfNibbles(0x7FU));
// Incorrect output. Correct output is:
//   0x5C: 17
//   0x85: -3
//   0xF2: 1
//   0x00: 0
//   0xFF: 14
//   0x70: 7
//   0x80: -8
//   0x7F: 22

   return 0;
}
