/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Task_2.c
 *    \author     Zsolt Balo
 *    \brief      Solutions for the Task 2 exercises 
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Task_2.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

// Use auto-formatter.

/**
 * \brief      Function which checks if a value passed as an argument is a palindrome.
// Specify parameter name (uc_Number : )
 * \param      Palindrom test argument
 * \return     If the argument is a palindrome, or if is not  
 */
boolean Task2_guc_VerifyPalindrome(uint8 uc_Number)
{
	if((uc_Number & 0x0F) == (((uc_Number & 0x10)>>1)|(uc_Number & 0x20>>3)|(uc_Number & 0x40>>5)|(uc_Number & 0x80)>>7))
	{
// The function shall return TRUE or FALSE. Get rid of multiple returns.
		return E_OK;
	}
	else
	{
		return E_NOT_OK;
	}
}

/**
 * \brief      Function which returns the number of bits that are 1 in the binary representation of a value passed as an argument.
// Specify parameter name (uc_Number : )
 * \param      Argument used for the bit counting.
 * \return     the sum of the bits, with the value 1.
 */
uint8 Task2_guc_CountBitsOf1(uint8 uc_Number)
{
// Naming convention not respected. Correct names are uc_Temp and uc_Index.
	uint8 uc_temp = 0x00;
	uint8 uc_index = 0x00;

	for(uc_index = 0x00;uc_index<8;uc_index++)
	{
		uc_temp += ((uc_Number>>uc_index) & 0x01);
	}
	return uc_temp;
}

/**
 * \brief      Function which checks if a value passed as an argument is a power of 2.
// Specify parameter name (uc_Number : )
 * \param      Argument used for the check.
 * \return     If the argument is a power of two, or not
 */
boolean Task2_guc_VerifyPowerOf2(uint8 uc_Number)
{
// Naming convention not respected. Correct name is uc_Temp.
	uint8 uc_temp = 0x00;

// A faster way is a bitwise and between uc_Number and uc_Number - 1.
	uc_temp = Task2_guc_CountBitsOf1(uc_Number);

	if(uc_temp == 0x01)
	{
// The function shall return TRUE or FALSE. Get rid of multiple returns.
		return E_OK;
	}
	else
	{
		return E_NOT_OK;
	}
}

/**
 * \brief      Function which calculates the sum between the low nibble (least significant 4 bits) and the high nibble (most significant 4 bits) of a byte value that is passed as an argument..
// Specify parameter name (uc_Number : )
 * \param      The byte of which parts will be used for the summation
 * \return     Result of the sum of the nibbles
 */
sint8 Task2_gsc_CalculateSumOfNibbles(uint8 uc_Byte)
{
// Naming convention not respected. Correct names are uc_LowNibble, uc_HighNibble and sc_Result.
	uint8 uc_low_nibble = 0x00;
	sint8 uc_result = 0x00, uc_high_nibble = 0x00;

	uc_low_nibble  = uc_Byte & 0x0F;
// AND mask not needed here. Shifting is enough.
	uc_high_nibble = (((uc_Byte) & 0xF0)>>4);

// A faster way would be if (uc_high_nibble >= 0x08)
	if((uc_high_nibble & 0x08)!=0x00)
	{
// Incorrect. The sign extension shall be performed before calculating the sum.
		uc_result = (uc_low_nibble + (uc_high_nibble)) | 0xF0;
	}
	else
	{
		uc_result = uc_low_nibble + uc_high_nibble;
	}

	return uc_result;
}
